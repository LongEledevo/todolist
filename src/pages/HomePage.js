import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Button,Modal } from "antd";
import LoadingComom from '../commom/LoadingCommom'

export default class HomePage extends Component {
  state = {
    isLoading: false,
    isModalOpen: true
  };
  render() {
    const wapper = {
      position: "fixed",
      width: "100vw",
      height: "100vh",
      top: 0,
      left: 0,
      zIndex: 1001,
      opacity: 0.9,
  }
 

    const handleCancel = () => {
      this.setState({isLoading:false})
    };
    return (
      <div>
        {this.state.isLoading ? <div style={wapper}></div> : ""}
        <div >HomePage</div>
        <NavLink to="/items">Chuyển trang</NavLink>
        <Button
          onClick={() => {
            this.setState({ isLoading: !this.state.isLoading });
          }}
        >
          Summit
        </Button>
          <Modal
            title="Basic Modal"
            open={this.state.isLoading}
            onCancel={handleCancel}
            footer={null}
          >
             {this.state.isLoading ? <LoadingComom/> : ""}
            <p>Title sẽ ở đây</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Footer sẽ ở đây</p>
          </Modal>
      </div>
    );
  }
}
