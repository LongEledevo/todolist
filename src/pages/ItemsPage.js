import React, { Component } from 'react'
import ItemContainer from '../container/ItemContainer'

export default class ItemsPage extends Component {
  render() {
    return (
      <div>
        <ItemContainer/>
      </div>
    )
  }
}
