
import * as types from "../contants"

const DEFAULT_STATE = {
    ListJob : [],
    isLoanding : false,
    mesage: "",
    error: false,
    errorMess : ""
}
export default function jobsReducer(state = DEFAULT_STATE, action) {
    switch (action.type) {
      case types.GET_JOBS_REQUEST: {
        return{
          ...state,
          isLoanding: true
        }
      }
      case types.GET_JOBS_SUCESS: {
        return {
          ...state,
          ListJob: action.payload.ListJob
        }
      }
      case types.GET_JOBS_FAILURE: {
        return{
          ...state,
          error: false,
          errorMess: action.payload.e
        }
      }
      default:
        return state
    }
  }
