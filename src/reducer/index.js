import {combineReducers} from "redux"
import jobsReducer from "./JobsReducer"

const rootReducer = combineReducers({
    // Define a top-level state field named `todos`, handled by `todosReducer`
    jobs: jobsReducer,
  })
  
  export default rootReducer