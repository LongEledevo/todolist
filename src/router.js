import { Routes, Route, BrowserRouter } from "react-router-dom";
import * as pages from "./pages/index";
import Layout from "./layout/index";
const RootRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Layout />}>
          <Route path="/" element={<pages.HomePage />}/>
          <Route path="/items" element={<pages.ItemsPage />}/>
          {/* <Route path="*" element={<NoMatch />}/> */}
        </Route>     
      </Routes>
    </BrowserRouter>
  );
};

export default RootRouter;
