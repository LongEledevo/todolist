import {all} from "redux-saga/effects"
import JobSaga from "./jobSaga"

export default function * rootSaga(){
    yield all([
        ...JobSaga
    ])
}