import { put, takeEvery } from "redux-saga/effects";
import * as types from "../contants";
import {getDataJob} from "../fetchAPI/fetchApi"

function* getJobSaga() {
  try {
    const res = yield getDataJob();
    yield put({
      type: types.GET_JOBS_SUCESS,
      payload: {
        ListJob: res.content
      },
    });
  } catch (error) {
    yield put({
      type: types.GET_JOBS_REQUEST,
      payload: error.message,
    });
  }
}

const JobSaga = [takeEvery(types.GET_JOBS_REQUEST, getJobSaga)];

export default JobSaga;
