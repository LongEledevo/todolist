import { notification } from 'antd';

const openNotification = (type,message,description) => {
    notification[type]({
      message: `Cập nhật thành công`,
      duration: 3,
      description:
        "Thông báo cập nhật thành công",
      placement:"topRight"
    });
  };
export {
    openNotification
}