import React, { Component } from "react";
import { Modal, Input, Spin } from "antd";
import "./ModalCommom.css";
import { Isremote } from "../contants";
import LoadingComom from './LoadingCommom'

export default class ModalComom extends Component {
  render() {
    console.log(this.props);
    let content;
    if (this.props.Title == Isremote.IsDelete) {
      content = (
        <p>
          Bạn có chắc chắn muốn xóa
          <span style={{ color: "red", marginLeft: "2px" }}>
            {this.props.inputValue}{" "}
          </span>
          không ?
        </p>
      );
    } else {
      content = (
        <>
          <label>Tên Công việc:</label>
          <Input
            onChange={this.props.onDataInputChange}
            value={this.props.inputValue}
          />
        </>
      );
    }
    return (
      <div>
        <Modal
          className={
            this.props.Title == Isremote.IsCreate
              ? "Modal-Create"
              : this.props.Title == Isremote.IsUpdate
              ? "Modal-Update"
              : "Modal-Delete"
          }
          okText={this.props.okText}
          title={
            this.props.Title == Isremote.IsCreate
              ? "Thêm công việc"
              : this.props.Title == Isremote.IsUpdate
              ? "Sửa công việc"
              : "Xóa công việc"
          }
          open={this.props.isOpen}
          onOk={this.props.handleOk}
          onCancel={this.props.handleClose}
          closable={!this.props.isLoading}
        >
           {this.props.isLoading ? <LoadingComom/> : ""}
            {content}
        </Modal>
      </div>
    );
  }
}
