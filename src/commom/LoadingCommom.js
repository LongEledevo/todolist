import { Spin } from "antd";

const OpenLoading = (props) => {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
    zIndex: 999,
  };
  const wapper = {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "#d2d2d2",
    zIndex: 998,
    opacity: 0.9,
  };
  return (
    <>
      <div style={wapper} >
        <Spin style={style}  />
      </div>
    </>
  );
};

export default OpenLoading;
