// import {domain,HTTP_HEADER} from "../contants"

// async function fetchApi(domain = {}, params = {}, body) {
//     try {
//         let currentParams = "";

//         if (params.page) {
//           currentParams = `?page=${params.page}&limit=${params.limit}`;
//         } else if (params.id) {
//           currentParams = `/${params.id}`;
//         } else {
//           currentParams = "";
//         }
    
//         const URL = `${domain.url}${currentParams}`;

//       const response = await fetch(URL, {
//         method: domain.method,
//         headers: HTTP_HEADER,
//         body: body && JSON.stringify(body),
//       });
  
//       const result = await response.json();
//       console.log("Success:", result);
//     } catch (error) {
//       console.error("Error:", error);
//     }
//   }
async function getDataJob() {
    try {
      const response = await fetch("http://localhost:8081/todo/getJob", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
  
      const result = await response.json();
      console.log("Success:", result);
      return result
    } catch (error) {
      console.error("Error:", error);
    }
  }

  export{
    getDataJob
  }