import React, { Component } from "react";
import { Button, Table, } from "antd";
import ModalComom from "../commom/ModalComom";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { Isremote } from "../contants";
import "../css/Items.css";
import { openNotification } from "../commom/NotificationCommom";



export default class ItemComponet extends Component {
  state = {
    isOpen: false,
    isLoading: false,
    title: "",
    value: "",
    okText: "",
  };

  render() {
    console.log(this.props);
    const ListJob = this.props.ListJob
    const handleOpen = (isRemote, record) => {
      console.log(record);
      if (isRemote == "thêm") {
        this.setState({
          isOpen: true,
          title: Isremote.IsCreate,
          okText: "Submit",
        });
      }
      if (isRemote == "sửa") {
        this.setState({
          isOpen: true,
          title: Isremote.IsUpdate,
          value: record,
          okText: "Update",
        });
      }
      if (isRemote == "xóa") {
        this.setState({
          isOpen: true,
          title: Isremote.IsDelete,
          value: record,
          okText: "Submit",
        });
      }
    };
    const handleClose = () => {
      this.setState({ isOpen: !this.state.isOpen, value: "" });
    };
    const handleOk = () => {
      console.log(this.state.value);
      this.setState({ isLoading: true });
      setTimeout(() => {
        this.setState({ isOpen: !this.state.isOpen });
        this.setState({ isLoading: !this.state.isLoading });
        this.setState({ value: "" });
        openNotification("error");
      }, 2000);
    };
    const handleDataChange = (e) => {
      this.setState({ value: e.target.value });
    };
    const wapper = {
      position: "fixed",
      width: "100vw",
      height: "100vh",
      top: 0,
      left: 0,
      zIndex: 1001,
      opacity: 0.9,
  }
    const columns = [
      {
        title: "STT",
        dataIndex: "id",
        key: "id",
        render: (text,record,index) => {
          return <Button type="link">{index + 1}</Button>
        }
      },
      {
        title: "Tên công việc",
        dataIndex: "nameJob",
        key: "nameJob",
      },
      {
        title: "Tiến độ",
        dataIndex: "process",
        key:"process"
      },
      {
        title: "Actions",
        key: "Actions",

        render: (record) => (
          <>
            <Button
              onClick={() => {
                handleOpen("sửa", record.nameJob);
              }}
            >
              <EditOutlined />
            </Button>
            <Button
              onClick={() => {
                handleOpen("xóa", record.nameJob);
              }}
              style={{ marginLeft: "12px" }}
            >
              <DeleteOutlined />
            </Button>
          </>
        ),
      },
    ];
    return (
      <div>
        {this.state.isLoading ? <div style={wapper}>ket</div> : ""}
        <div className="header">
          <h1>Chào mừng đến với trang Items</h1>
        </div>
        <div className="button-create">
          <Button onClick={() => handleOpen("thêm")}>Thêm</Button>
        </div>
        <div>
          <ModalComom
            isOpen={this.state.isOpen}
            handleClose={handleClose}
            handleOk={handleOk}
            isLoading={this.state.isLoading}
            Title={this.state.title}
            onDataInputChange={handleDataChange}
            inputValue={this.state.value}
            okText={this.state.okText}
          />          
        </div>
        <Table 
        columns={columns} 
        dataSource={ListJob} 
        rowKey="id"
        />
      </div>
    );
  }
}
