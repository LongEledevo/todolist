import React, { Component } from 'react'
import ItemComponet from '../components/ItemComponet'
import * as actions from "../actions/jobActions"
import {connect} from "react-redux"


class ItemContainer extends Component {
  componentDidMount(){
    this.props.getJob();
  }
  render() {
    return (
      <ItemComponet {...this.props}/>
    )
  }
}

const mapStateToProp = (state) => {
  return{
    ListJob : state.jobs?.ListJob
  }
}
const mapDispathcToProps = (dispatch) => {
  return {
    getJob: () => dispatch(actions.getJobRequest())
  }
}

export default connect(mapStateToProp,mapDispathcToProps)(ItemContainer)
